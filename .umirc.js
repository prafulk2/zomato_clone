import routerConfig from "./config/routerConfig";
import os from 'os';

// ref: https://umijs.org/config/
export default {
  treeShaking: true,
  routes: routerConfig,
  cssLoaderOptions: {
    localIdentName: '[local]',
  },
  plugins: [
    // ref: https://umijs.org/plugin/umi-plugin-react.html
    ['umi-plugin-react',
    {
      antd: true,
      dva:true,
      dynamicImport: true,
      title: 'Zomato',
       ...(!process.env.TEST && os.platform() === 'darwin'
          ? {
            dll: {
              include: ['dva', 'dva/router', 'dva/saga', 'dva/fetch'],
              exclude: ['@babel/runtime'],
            },
            hardSource: false,
          }
          : {}),
        locale: { 
          enable: true,
          default: 'en-US',
        },
        routes: {
          exclude: [
            /models\//,
            /services\//,
            /model\.(t|j)sx?$/,
            /service\.(t|j)sx?$/,
            /components\//,
          ],
        },
    }],
  ],
}
