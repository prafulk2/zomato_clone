// import { Redirect } from "umi";

export default [
    {
        path: '/',
        component: '../layouts/index',
        routes: [
            {
                path: '/', component: '../pages/Home/Home.js' 
            },            
            {
                path: '/order', component: '../pages/Order/Order.js' 
            },
            {
                path: '/dining', component: '../pages/DiningOut/DiningOut.js' 
            }, 
            {
                path: '/nightlife', component: '../pages/NightLife/NightLife.js' 
            },
            {
                path: '/tabs', component: '../pages/Tabs/Tab.js' 
            },
            // {
            //     path: '/tabs/:id', component: '../pages/Tabs/Tab.js' 
            // },
            {
                path:'/demo', component: '../pages/Demo.js' 
            },
            {
               component:'../pages/404.js'
            }
            // <Redirect to='/tabs' />
        ]
        
    }
]