import { Layout } from 'antd';
import './index.scss'

const { Content } = Layout;
function BasicLayout(props) {
  return (
    <Layout className="layout">
      <Content>
        {props.children}
      </Content>
    </Layout>
  );
}

export default BasicLayout;
