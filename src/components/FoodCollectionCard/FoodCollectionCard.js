import React from 'react'
import { Typography } from 'antd'
import './FoodCollectionCard.scss'

const { Paragraph } = Typography
function FoodCollectionCard(props) {
    const data = props.obj
    return (
        <div
            className="card"
        >
            <img alt="example" className="img" src={data?.img} />
            <div className="meta">
                <Paragraph className="title" ellipsis={{ rows: 1 }}>
                    {data?.title}
                </Paragraph>
                <Paragraph className="para" ellipsis={{ rows: 1 }}>
                    {data?.place}
                </Paragraph>
            </div>
        </div>
    )
}

export default FoodCollectionCard
