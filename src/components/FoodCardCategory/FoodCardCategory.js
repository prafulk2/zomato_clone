import React from 'react'
import { Card, Col, Row, Space, Typography } from 'antd';
import { StarFilled } from '@ant-design/icons'
import './FoodCardCategory.scss'
const { Title, Paragraph } = Typography

function FoodCardCategory(props) {
    const data = props.data
    return (
        <div>
            <Row className="Row4FoodCardCategory" justify="center">
                <Col span="24">
                    <Row className="CardRow" >
                        <Col span="24">
                            <Card
                                hoverable
                                className="card"
                                cover={<img alt="example" className="img" src={data?.img} />}
                            >
                                <div className="discountdiv">
                                    <div className="mindiv">
                                    {data.dist}
                                    </div>
                                </div>
                                <div className="titlediv">
                                    <div className="Ttdiv">
                                        <Title className="title">{data.title}</Title>
                                    </div>
                                    <div className="badge">
                                        <Space className="space" >
                                            <Paragraph className="para">3.9</Paragraph>
                                            <div className="stardiv">
                                                <StarFilled className="staricon" />
                                            </div>
                                        </Space>
                                    </div>
                                </div>
                                <div className="description_div">
                                    <div className="Desc_1">
                                        <Paragraph className="para" ellipsis={{ rows: 1 }}>Mithai, Street Food Mithai, Street Food </Paragraph>
                                    </div>
                                    <div className="Desc_2">
                                        <Paragraph className="para" ellipsis={{ rows: 1 }}>
                                            ₹100 for one
                                        </Paragraph>
                                    </div>

                                </div>
                                <Row className="descRow">
                                    <Col span="24">
                                        <Paragraph className="Para">{data.desc}</Paragraph>
                                    </Col>
                                </Row>
                            </Card>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </div>
    )
}

export default FoodCardCategory
