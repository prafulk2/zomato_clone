import { Col, Row, Space, Typography, Divider, Button, Popover} from 'antd';
import React from 'react';
import footerimg1 from '../../assets/footerimg1.webp';
import { FacebookFilled, FlagOutlined,InstagramFilled, TwitterCircleFilled, DownOutlined } from '@ant-design/icons';
import './Footer.scss';
import appstore1 from '../../assets/appstore1.webp'
import appstore2 from '../../assets/appstore2.webp'
const { Title, Paragraph } = Typography;
function Footer() {
    const Lang = ['English', 'shqip', 'বাংলা', '日本語', 'नेपाली', 'ગુજરાતી', '	हिन्दी','Português (BR)']

    // for language popover
    const RenderLanguage = () => {
        return <div>
            {Lang.map((val, index) => {
                return <div key={index}>
                    <Paragraph>
                    {val}
                    </Paragraph>
                </div>
            })}
        </div>
    }

    // for contry Popover
    const countryList = ["Afghanistan ", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas (the)", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan"]
    const RenderCountry=()=>{
        return (
            <>
              <Row style={{width:'400px'}} gutter={[12,5]}>
              {
                    countryList.map((val, index) => <Col span="8" key={index}>
                        <Space style={{width:"100%",overflow:'hidden'}}> 
                        <FlagOutlined className="icons" />
                        <Paragraph className="country-name" style={{margin:0 ,overflow:'hidden'}} ellipsis={{ rows: 1 }}>
                            {val}
                        </Paragraph>
                        </Space>
                    </Col>
                    )
                }
              </Row>
            </>
        )
    }
    return (
        <div className="footer_Container">
            <Row justify="center" className="Row1">
                <Col xl={19} sm={22} xs={22}>
                    <Row className="innerRow1" gutter={[32, 32]}>
                        <Col xl={18} sm={14} xs={24}>
                            <img src={footerimg1} alt="" className="img" />
                        </Col>
                        <Col xl={6} sm={10} xs={24}>
                            <Space className="Space">
                                <Popover trigger="click" content={<RenderCountry />}>
                                    <Button className="btn" icon={<img src="https://img.icons8.com/color/48/000000/india.png" alt="imgicon" className="imgicon" />}><span className="span">India</span> <DownOutlined className="DownOutlined" /></Button>
                                </Popover>
                                <div className="language-popover">
                                <Popover trigger="click" content={<RenderLanguage />}>
                                    <Button  className="btn" icon={<img src="https://cdn-icons-png.flaticon.com/512/44/44645.png" alt="imgicons" className="imgicon" />}><span className="span">English</span> <DownOutlined className="DownOutlined" /></Button>
                                </Popover>
                                </div>  
                            </Space>
                        </Col>
                    </Row>
                    <div className="itemmaindiv">
                        <div className="div1">
                            <Space direction="vertical" className="Space">
                                <Title className="title" level={5}  >COMPANY</Title>
                                <a href="https://google.com" className="para">Who We are </a>
                                <a href="https://google.com" className="para">Blog </a>
                                <a href="https://google.com" className="para">Careers </a>
                                <a href="https://google.com" className="para">Report Fraud </a>
                                <a href="https://google.com" className="para">Contact </a>
                                <a href="https://google.com" className="para">Investor Relations </a>
                            </Space>
                        </div>
                        <div className="div2">
                            <Space direction="vertical" className="Space">
                                <Title className="title" level={5}  >FOR FOODIES</Title>
                                <a href="https://google.com" className="para"> Code of Conduct  </a>
                                <a href="https://google.com" className="para"> Community  </a>
                                <a href="https://google.com" className="para"> Blogger Help  </a>
                                <a href="https://google.com" className="para"> Mobail Apps </a>
                            </Space>
                        </div>
                        <div className="div3">
                            <Space direction="vertical" className="Space" >
                                <Title className="title" level={5}  >FOR RESTAURANTS</Title>
                                <a href="https://google.com" className="para"> Add REstaurant  </a>
                                <br className="br" />
                                <Title className="title" level={5}  >FOR ENTERPRISES</Title>
                                <a href="https://google.com" className="para"> Add REstaurant  </a>
                            </Space>
                        </div>
                        <div className="div4">
                            <Space direction="vertical" className="Space">
                                <Title className="title" level={5}  >FOR YOU</Title>
                                <a href="https://google.com" className="para"> Privacy  </a>
                                <a href="https://google.com" className="para"> Terms  </a>
                                <a href="https://google.com" className="para"> Security  </a>
                                <a href="https://google.com" className="para"> sitemap  </a>
                                <a href="https://google.com" className="para">   </a>
                                <a href="https://google.com" className="para">   </a>
                                <a href="https://google.com" className="para">   </a>
                            </Space>
                        </div>
                        <div className="div5">
                            <Space direction="vertical" className="Space">
                                <Title className="title" level={5}  >SOCIAL LINKS</Title>
                                <Space>
                                    <FacebookFilled className="icons" />
                                    <TwitterCircleFilled className="icons" />
                                    <InstagramFilled className="icons" />
                                </Space>
                                <Row gutter={[24, 24]} className="ImgRow">
                                    <Col xl={16} xs={12} sm={16}>
                                        <img src={appstore1} alt="appstore" className="appstoreimg" />
                                    </Col>
                                    <Col xl={16} xs={12} sm={16}>
                                        <img src={appstore2} alt="appstore" className="appstoreimg" />
                                    </Col>
                                </Row>
                            </Space>
                        </div>
                        <div className="div6">

                        </div>
                    </div>
                    <Divider />
                    <Row className="Rows2">
                        <Col>
                            <Paragraph className="para">
                                By continuing past this page, you agree to our Terms of Service, Cookie Policy, Privacy Policy and Content Policies. All trademarks are properties of their respective owners. 2008-2021 © Zomato™ Ltd. All rights reserved.
                            </Paragraph>
                        </Col>
                    </Row>
                </Col>
            </Row>
            {/* <Footer /> */}
        </div>
    );
}
export default Footer;
