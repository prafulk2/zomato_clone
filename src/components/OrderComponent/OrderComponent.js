import React, { useState } from 'react'
import footerimg1 from '../../assets/footerimg1.webp';
import { Link, NavLink } from "umi";
import { EnvironmentFilled, MenuOutlined, CloseOutlined } from '@ant-design/icons'
import './OrderComponent.scss'
import { Row, Col, Form, Select, Space, Input, Button } from 'antd';

function OrderComponent() {
    const [toggle, seTtoggle] = useState(false)
    const handleToggle = () => {
        seTtoggle(!toggle)
    }
    return (
        <div className="ordercmp_Container">
            <div className="maindiv">
                <div className="icondiv">
                    {toggle ? <CloseOutlined className="commanIcon" onClick={handleToggle} /> : <MenuOutlined className="commanIcon" onClick={handleToggle} />}
                </div>
                <div className="imgdiv">
                    <Link to="/">
                        <img src={footerimg1} className="img" alt="" />
                    </Link>
                </div>
                <div className={toggle ? "toggle-item-div" : "toggle-item-div toggle-close"} onClick={handleToggle}>
                    <Space direction="vertical" className="space">
                        <p className="para para1">
                            Log in
                        </p>
                        <p className="para">
                            Sign up
                        </p>
                    </Space>
                </div>
                <div className="MobailView">
                    <div className="fulldiv">
                        <Space>
                            <div className="appbtn">
                                <Button className="btn">Use App</Button>
                            </div>
                            <div className="userIcon">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="#EF4F5F" width="18" height="18" viewBox="0 0 20 20" aria-labelledby="icon-svg-title- icon-svg-desc-" role="img" className="sc-rbbb40-0 iwHbVQ"><circle cx="10" cy="4.5" r="4.5"></circle><path d="M18.18,14.73c-2.35-4.6-6.49-4.48-8.15-4.48s-5.86-.12-8.21,4.48C.59,17.14,1.29,20,4.54,20H15.46C18.71,20,19.41,17.14,18.18,14.73Z"></path></svg>
                            </div>
                        </Space>
                    </div>
                </div>
                <div className="itemDiv">
                    <div className="innerdiv">
                        <Row>
                            <Col span="8" className="Col1" align="start">
                                <div className="itemdiv">
                                    <EnvironmentFilled className="icon" />
                                    <Form className="form">
                                        <Form.Item >
                                            <Select placeholder="BJS colony" >
                                            </Select>
                                        </Form.Item>
                                    </Form>
                                </div>
                            </Col>
                            <Col span="16" className="Col2">
                                <span className="vrBorder"></span>
                                <Space>
                                    <Form>
                                        <Form.Item >
                                            <Input size="large" placeholder="Search for restaurant, cuisine or a dish" prefix={<div className="input-prefix">
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="#828282" width="18" height="18" viewBox="0 0 20 20" aria-labelledby="icon-svg-title- icon-svg-desc-" role="img" class="sc-rbbb40-0 iwHbVQ"><title>Search</title><path d="M19.78 19.12l-3.88-3.9c1.28-1.6 2.080-3.6 2.080-5.8 0-5-3.98-9-8.98-9s-9 4-9 9c0 5 4 9 9 9 2.2 0 4.2-0.8 5.8-2.1l3.88 3.9c0.1 0.1 0.3 0.2 0.5 0.2s0.4-0.1 0.5-0.2c0.4-0.3 0.4-0.8 0.1-1.1zM1.5 9.42c0-4.1 3.4-7.5 7.5-7.5s7.48 3.4 7.48 7.5-3.38 7.5-7.48 7.5c-4.1 0-7.5-3.4-7.5-7.5z"></path></svg>
                                            </div>} />
                                        </Form.Item>
                                    </Form>
                                </Space>
                            </Col>
                        </Row>
                    </div>
                    <ul className="nav-menu">
                        <li className="nav-item">
                            <NavLink
                                exact
                                to="/todo"
                                activeClassName="active"
                                className="nav-links login"
                            >
                                Log in
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink
                                exact
                                to="/todo"
                                activeClassName="active"
                                className="nav-links"
                            >
                                Sign up
                            </NavLink>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}
export default OrderComponent