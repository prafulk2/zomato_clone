import React from 'react'
import { Card, Col, Divider, Row, Space, Typography } from 'antd';
import stock1 from '../../assets/stocklogo.webp'
import stock2 from '../../assets/stocklogo2.webp'
import { StarFilled } from '@ant-design/icons'
import './FoodCard.scss'

const { Title, Paragraph } = Typography
function FoodCard(props) {
    const data = props.data
    return (
        <div>
            <Row className="Row4" justify="center">
                <Col span="24">
                    <Row className="CardRow" >
                        <Col span="24">
                            <Card
                                hoverable
                                className="card"
                                cover={<img alt="example" className="img" src={data?.img} />}
                            >
                                <div className="discountdiv">
                                    <div className="disDiv">
                                        30% OFF
                                    </div>
                                    <div className="mindiv">
                                        36min
                                    </div>
                                </div>
                                <div className="titlediv">
                                    <div className="Ttdiv">
                                        <Title className="title">{data.title}</Title>
                                    </div>
                                    <div className="badge">
                                        <Space className="space" >
                                            <Paragraph className="para">3.9</Paragraph>
                                            <div className="stardiv">
                                                <StarFilled className="staricon" />
                                            </div>
                                        </Space>
                                    </div>
                                </div>
                                <div className="description_div">
                                    <div className="Desc_1">
                                        <Paragraph ellipsis={{ rows: 1 }}>Mithai, Street Food Mithai, Street Food </Paragraph>
                                    </div>
                                    <div className="Desc_2">
                                        <Paragraph ellipsis={{ rows: 1 }}>
                                            ₹100 for one
                                        </Paragraph>
                                    </div>
                                </div>
                                <Divider className="divider" />
                                <div className="Card_foot">
                                    <div className="Desc_1">
                                        <div className="imgdiv">
                                            <img src={stock1} alt="img1" className="img" />
                                        </div>
                                        <div className="div2">
                                            <Paragraph className="para">Mithai, Street Food Mithai, Street Food </Paragraph>
                                        </div>
                                    </div>
                                    <div className="Desk_2">
                                        <img src={stock2} alt="img2" className="img2" />
                                    </div>
                                </div>
                            </Card>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </div>
    )
}

export default FoodCard
