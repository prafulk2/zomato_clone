import React, { useState } from 'react'
import { Button, Carousel, Col, Row, Space, Popover, Tag, Affix, Tabs, Modal, Radio, Form, Checkbox, Slider, Input, Typography, Divider } from 'antd';
import { SearchOutlined, CloseOutlined, DownOutlined } from '@ant-design/icons'
import FoodCollectionCard from '../../components/FoodCollectionCard/FoodCollectionCard';
import cards1img from '../../assets/card1img.webp'
import cards2img from '../../assets/card2img.webp'
import cards3img from '../../assets/card3img.webp'
import cards4img from '../../assets/card4img.webp'

import cardimg1 from '../../assets/cardimg1.avif'
import cardimg2 from '../../assets/cardimg2.avif'
import cardimg3 from '../../assets/cardimg3.avif'
import cardimg4 from '../../assets/cardimg4.avif'
import cardimg5 from '../../assets/cardimg5.avif'
import cardimg6 from '../../assets/cardimg6.avif'

import './DiningOut.scss'
import FoodCardCategory from '../../components/FoodCardCategory/FoodCardCategory';
const { TabPane } = Tabs;
const { Title, Paragraph } = Typography

const { CheckableTag } = Tag;
function DiningOut() {
    const SampleNextArrow = props => {
        const { onClick } = props
        return (
            <div
                className="nextArrow"
                onClick={onClick}
            >
                <div className="div_previcon">
                    <img src="https://cdn-icons-png.flaticon.com/512/860/860828.png" className="imgicon" alt="" />
                </div>
            </div>
        )
    }
    const SamplePrevArrow = props => {
        const { onClick } = props
        return (
            <div
                className="prevArrow"
                onClick={onClick}
                style={{ height: "100%", width: "40px" }}>
                <div className="div_previcon">
                    <img src="https://cdn-icons-png.flaticon.com/512/860/860790.png" className="imgicon" alt="" />
                </div>
            </div>
        )
    }
    const carddata = [
        { img: cardimg1, dist: "4.7 km", desc: 'Cantt Area, Jodhpur', title: "Pizza Hut" },
        { img: cardimg2, dist: "8.3 km", desc: 'Ratanada, jodhpur', title: "Janta Sweet Home-Food Court" },
        { img: cardimg3, dist: "5.2 km", desc: 'Station Road, Jodhpur', title: "Dimsum Express" },
        { img: cardimg4, dist: "4.1 km", desc: 'Umiad Bhawan Palace,Cantt Area, Jodhpur', title: "Janta Sweet Home" },
        { img: cardimg5, dist: "2.6 km", desc: 'Air Force Area, Jodhpur', title: "The Rockyard" },
        { img: cardimg6, dist: "8.6 km", desc: 'Shastri Nagar,Jodhpur', title: "Shri Pokar Sweet" },
        { img: cardimg1, dist: "4.7 km", desc: 'Cantt Area, Jodhpur', title: "Pizza Hut" },
        { img: cardimg2, dist: "8.3 km", desc: 'Ratanada, jodhpur', title: "Janta Sweet Home-Food Court" },
        { img: cardimg3, dist: "5.2 km", desc: 'Station Road, Jodhpur', title: "Dimsum Express" },
        { img: cardimg4, dist: "4.1 km", desc: 'Umiad Bhawan Palace,Cantt Area, Jodhpur', title: "Janta Sweet Home" },
        { img: cardimg5, dist: "2.6 km", desc: 'Air Force Area, Jodhpur', title: "The Rockyard" },
        { img: cardimg6, dist: "8.6 km", desc: 'Shastri Nagar,Jodhpur', title: "Shri Pokar Sweet" },
    ]
    const arr = [
        {
            img: cards1img,
            title: "Best of Jodhpur",
            place: "10 Places "
        },
        {
            img: cards2img,
            title: "Trading This Week",
            place: "25 Places "
        },
        {
            img: cards3img,
            title: "Sweet Tooth",
            place: "12 Places "
        },
        {
            img: cards4img,
            title: "Roayl Rajasthani Cuisine",
            place: "12 Places "
        },
        {
            img: cards2img,
            title: "Trading This Week",
            place: "25 Places "
        },
        {
            img: cards3img,
            title: "Sweet Tooth",
            place: "12 Places "
        },
        {
            img: cards4img,
            title: "Roayl Rajasthani Cuisine",
            place: "12 Places "
        }
    ]
    const settings = {
        cssEase: 'linear',
        autoplaySpeed: 4000,
        dots: false,
        infinite: true,
        speed: 3000,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: <SamplePrevArrow />,
        nextArrow: <SampleNextArrow />,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 2,
                    initialSlide: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false,
                    arrows: false
                }
            }
        ]
    };

    // Model Code
    const tagsData = {
        tagdata0: [
            {
                tag: 'Distance',
                icon: <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="#9C9C9C"
                    width="16"
                    height="14"
                    viewBox="0 0 20 20"
                    aria-labelledby="icon-svg-title- icon-svg-desc-"
                    role="img"
                    className="sc-rbbb40-0 iwHbVQ"
                >
                    <path d="M9.9,14.8l-2.3,2.3V9c0-0.5-0.4-0.8-0.8-0.8S5.9,8.5,5.9,9v8.2l-2.3-2.3c0,0,0,0,0,0c-0.3-0.3-0.9-0.3-1.2,0 c-0.3,0.3-0.3,0.8,0,1.2l3.7,3.7c0.3,0.3,0.8,0.3,1.2,0l3.7-3.7c0.2-0.2,0.2-0.4,0.2-0.6c0-0.2-0.1-0.4-0.2-0.6 C10.8,14.5,10.3,14.5,9.9,14.8z"></path>
                    <path d="M17.6,4l-3.7-3.7c-0.3-0.3-0.8-0.3-1.2,0L8.9,4C8.7,4.1,8.6,4.4,8.7,4.6c0,0.2,0.1,0.4,0.2,0.6c0.3,0.3,0.8,0.3,1.2,0 l2.3-2.3V11c0,0.5,0.4,0.8,0.8,0.8c0.5,0,0.8-0.4,0.8-0.8V2.9l2.3,2.3c0,0,0,0,0,0c0.3,0.3,0.9,0.3,1.2,0C17.9,4.8,17.9,4.3,17.6,4 z"></path>
                </svg>
            },
            {
                tag: 'Rating 4.0+',
                icon: ""
            }, {
                tag: 'Online booking',
                icon: ""
            },
        ],
        tagdata1: [
            {
                tag: 'Outdoor Seating',
                icon: ""
            },
            {
                tag: 'Serves Alcohol',
                icon: ""
            },
            {
                tag: 'Pure veg',
                icon: ""
            },
            {
                tag: 'Open Now',
                icon: ""
            },

        ],
        tagdata2: [
            {
                tag: 'Cafes',
                icon: ""
            },
            {
                tag: 'Fine Dining',
                icon: ""
            }

        ]

    };
    const [visible, setVisible] = useState(false);
    const showModal = () => {
        setVisible(true)
    };
    const plainOptions = ['Afghan', 'American', 'Arebian', 'Afghanistan', 'Albania', 'Algeria', 'American Samoa', 'Andorra', 'Angola', 'Anguilla', 'Antarctica', 'Antigua ', 'Argentina', 'Armenia', 'Aruba', 'Australia', 'Austria', 'Azerbaijan', 'Bahamas (the)', 'Bahrain', 'Bangladesh', 'Barbados', 'Belarus', 'Belgium', 'Belize', 'Benin', 'Bermuda', 'Bhutan',];

    const [checkedList, setCheckedList] = useState();
    const [Sliders, setSliders] = useState('');
    const [Ratings, setRatings] = useState(3)
    const [moreFilter, setMoreFilter] = useState('')

    const handleOk = () => {
        setVisible(false)
    };
    const handleCancel = () => {
        setVisible(false)
    };
    const [value, setValue] = useState('Popularity');
    const handleChange = e => {
        setValue(e.target.value);
    };
    const handleMoreFilter = (e) => {
        console.log(e);
        setMoreFilter(e)
    }

    const onChange = list => {
        setCheckedList(list);
    };
    const handleSlider = data => {
        setSliders(data);
    };
    const handleRating = (data) => {
        console.log(data);
        setRatings(data)
    }
    const marks = {
        3: {
            label: <strong>Any</strong>,
        },
        3.5: '3.5',
        4: '4.0',
        4.5: '4.5',
        5: '5.0',
    };

    // MoreFilter
    const RenderMoreFilter = () => {
        return <div className="MoreFilter_Container">
            <Input
                className="MoreFilter_Input"
                prefix={<svg xmlns="http://www.w3.org/2000/svg" fill="#B5B5B5" width="17" height="17" viewBox="0 0 20 20" aria-labelledby="icon-svg-title- icon-svg-desc-" role="img" class="sc-rbbb40-0 fajqkJ"><title>Search</title><path d="M19.78 19.12l-3.88-3.9c1.28-1.6 2.080-3.6 2.080-5.8 0-5-3.98-9-8.98-9s-9 4-9 9c0 5 4 9 9 9 2.2 0 4.2-0.8 5.8-2.1l3.88 3.9c0.1 0.1 0.3 0.2 0.5 0.2s0.4-0.1 0.5-0.2c0.4-0.3 0.4-0.8 0.1-1.1zM1.5 9.42c0-4.1 3.4-7.5 7.5-7.5s7.48 3.4 7.48 7.5-3.38 7.5-7.48 7.5c-4.1 0-7.5-3.4-7.5-7.5z"></path></svg>}
                suffix={<CloseOutlined />}
                placeholder="Search here"
            />
            <div className="Checkbox_body" >
                <Checkbox.Group style={{ width: '100%' }}>
                    <Row gutter={[0, 12]} >
                        {plainOptions.map((val, index) => {
                            return <Col key={index} xl={24} sm={24} xs={24}>
                                <Checkbox value={val}>{val}</Checkbox>
                            </Col>
                        })}
                    </Row>
                </Checkbox.Group>
            </div>
            <Divider />
            <Row className="MoreFilter-footer">
                <Col span="24" align="end">
                    <Space >
                        <Button type='text'>
                            Clear all
                        </Button>
                        <Button type='primary' className="btn">
                            Apply
                        </Button>
                    </Space>
                </Col>
            </Row>
        </div>
    }

    // code for tag it's css in layout/index.scss
    const [selectedTags, setSelectedTags] = useState(['Books'])
    const handleChangetag = (tag, checked) => {
        const nextSelectedTags = checked ? [...selectedTags, tag] : selectedTags.filter(t => t !== tag);
        setSelectedTags(nextSelectedTags);
    }
    const RenderHotTegs = (props) => {
        const obj = props.obj
        return <div className="Tag-Container">
            <CheckableTag
                checked={selectedTags.indexOf(obj.tag) > -1}
                onChange={checked => handleChangetag(obj.tag, checked)}
            >
                <div className="Tag-div">
                    {obj.icon}
                    {obj.tag}
                </div>
            </CheckableTag>
        </div>
    }

    // For Cuisines Popover
    const RenderCuisinePopover = () => {
        const plainOptions = ['Afghan', 'American', 'Arebian', 'Afghanistan', 'Albania', 'Algeria', 'American Samoa', 'Andorra', 'Angola', 'Anguilla', 'Antarctica', 'Antigua ', 'Argentina', 'Armenia', 'Aruba', 'Australia', 'Austria', 'Azerbaijan', 'Bahamas (the)', 'Bahrain', 'Bangladesh', 'Barbados', 'Belarus', 'Belgium', 'Belize', 'Benin', 'Bermuda', 'Bhutan'];
        return (
            <div className="Cuisines_Conainer">
                <Input
                    className="Cuisines_Input"
                    prefix={<SearchOutlined />}
                    suffix={<CloseOutlined />}

                />
                <div className="Checkbox_div" style={{ width: '300px' }}>
                    <Checkbox.Group style={{ width: '100%' }}>
                        <Row gutter={[0, 12]} >
                            {plainOptions.map((val, index) => {
                                return <Col key={index} xl={12} sm={12} xs={24}>
                                    <Checkbox value={val}>{val}</Checkbox>
                                </Col>
                            })}
                        </Row>
                    </Checkbox.Group>
                </div>
                <Divider />
                <Row className="Cuisines-footer">
                    <Col span="24" align="end">
                        <Space >
                            <Button type='text'>
                                Clear all
                            </Button>
                            <Button type='primary' className="btn">
                                Apply
                            </Button>
                        </Space>
                    </Col>
                </Row>
            </div>
        )
    }
    return (
        <div className="Dining_Container">
            <div className="FilterModel-container">
                <Modal
                    title={<Title className="Model_Title">Filters</Title>}
                    className="modal"
                    visible={visible}
                    onOk={handleOk}
                    onCancel={handleCancel}
                    footer={[
                        <Button key="back" className="Clear_All" onClick={handleCancel}>
                            Clear all
                        </Button>,
                        <Button key="submit" type="primary" onClick={handleOk} className="Apply_btn">
                            Apply
                        </Button>,
                    ]}
                >

                    <Tabs tabPosition="left">
                        <TabPane
                            tab={
                                <div className="Tab">
                                    <p align="start" className="title">
                                        Sort by
                                    </p>
                                    <p className="para">{value}</p>
                                </div>
                            }
                            key="1"
                        >
                            <Form className="Form" initialValues={{ radiogroup: value }}>
                                <Form.Item name="radiogroup">
                                    <Radio.Group value={value} onChange={handleChange}>
                                        <Space direction="vertical" className="SortBy_Space">
                                            <Radio value="Popularity">Popularity</Radio>
                                            <Radio value="Rating:High to Low">Rating:High to Low</Radio>
                                            <Radio value="Delivery Time">Delivery Time</Radio>
                                            <Radio value="Cost: Low to High">Cost: Low to High</Radio>
                                            <Radio value="Cost:High to Low">Cost:High to Low</Radio>
                                        </Space>
                                    </Radio.Group>
                                </Form.Item>
                            </Form>
                        </TabPane>
                        <TabPane
                            tab={
                                <div className="Tab">
                                    <p align="start" className="title">
                                        Cuisines
                                    </p>
                                    <p className="para">{checkedList ? `${checkedList?.length} Selected` : ''}</p>
                                </div>
                            }
                            key="2"
                        >
                            <div className="Filter_Cuisines">
                                <Input
                                    className="MoreFilter_Input"
                                    prefix={<svg xmlns="http://www.w3.org/2000/svg" fill="#B5B5B5" width="17" height="17" viewBox="0 0 20 20" aria-labelledby="icon-svg-title- icon-svg-desc-" role="img" class="sc-rbbb40-0 fajqkJ"><title>Search</title><path d="M19.78 19.12l-3.88-3.9c1.28-1.6 2.080-3.6 2.080-5.8 0-5-3.98-9-8.98-9s-9 4-9 9c0 5 4 9 9 9 2.2 0 4.2-0.8 5.8-2.1l3.88 3.9c0.1 0.1 0.3 0.2 0.5 0.2s0.4-0.1 0.5-0.2c0.4-0.3 0.4-0.8 0.1-1.1zM1.5 9.42c0-4.1 3.4-7.5 7.5-7.5s7.48 3.4 7.48 7.5-3.38 7.5-7.48 7.5c-4.1 0-7.5-3.4-7.5-7.5z"></path></svg>}
                                    suffix={<CloseOutlined />}
                                    placeholder="Search here"
                                />
                            </div>
                            <Form>
                                <Form.Item>
                                    <Checkbox.Group style={{ width: '100%' }} onChange={onChange}>
                                        <Row >
                                            {plainOptions.map((val, index) => {
                                                return <Col key={index} xl={12} sm={12} xs={24}>
                                                    <Checkbox value={val}>{val}</Checkbox>
                                                </Col>
                                            })}
                                        </Row>
                                    </Checkbox.Group>
                                </Form.Item>
                            </Form>
                        </TabPane>
                        <TabPane
                            tab={
                                <div className="Tab">
                                    <p align="start" className="title">
                                        Rating
                                    </p>
                                    <p className="para"></p>
                                </div>
                            }
                            key="4"
                        >
                            Rating
                            <Title level={4}>{Ratings !== 3 ? `${Ratings}+` : 'Any'}</Title>

                            <div className="slider_div">
                                <Slider
                                    min={3}
                                    max={5}
                                    reverse={false} className="slider slider_desktop" onChange={handleRating} marks={marks} step={null} defaultValue={100}
                                />
                                <Slider
                                    min={3}
                                    max={5}
                                    reverse={false} vertical className="slider slider_mobile" onChange={handleRating} marks={marks} step={null} defaultValue={100}
                                />
                            </div>
                        </TabPane>
                        <TabPane
                            tab={
                                <div className="Tab">
                                    <p align="start" className="title">
                                        Cost per person
                                    </p>
                                    <p className="para"></p>
                                </div>
                            }
                            key="3"
                        >
                            Cost per person
                            <Title level={4}>{Sliders.length > 0 ? `₹ ${Sliders[0]} - ₹ ${Sliders[1]} ` : 'Any'}</Title>
                            <div className="slider_div">
                                <Slider
                                    className="slider slider_desktop"
                                    onChange={handleSlider}
                                    range={{ draggableTrack: true }}
                                    defaultValue={[0, 100]}
                                />
                                <Slider
                                   className="slider slider_mobile"
                                    onChange={handleSlider}
                                    range={{ draggableTrack: true }}
                                    defaultValue={[0, 100]}
                                    vertical
                                />
                            </div>
                        </TabPane>
                        <TabPane
                            tab={
                                <div className="Tab">
                                    <p align="start" className="title">
                                        More Filters
                                    </p>
                                    <p className="para">{moreFilter ? `${moreFilter.length} Selected` : ""}</p>
                                </div>
                            }
                            key="5"
                        >
                            <div className="Filter_Morefilter">
                                <Input
                                    className="MoreFilter_Input"
                                    prefix={<svg xmlns="http://www.w3.org/2000/svg" fill="#B5B5B5" width="17" height="17" viewBox="0 0 20 20" aria-labelledby="icon-svg-title- icon-svg-desc-" role="img" class="sc-rbbb40-0 fajqkJ"><title>Search</title><path d="M19.78 19.12l-3.88-3.9c1.28-1.6 2.080-3.6 2.080-5.8 0-5-3.98-9-8.98-9s-9 4-9 9c0 5 4 9 9 9 2.2 0 4.2-0.8 5.8-2.1l3.88 3.9c0.1 0.1 0.3 0.2 0.5 0.2s0.4-0.1 0.5-0.2c0.4-0.3 0.4-0.8 0.1-1.1zM1.5 9.42c0-4.1 3.4-7.5 7.5-7.5s7.48 3.4 7.48 7.5-3.38 7.5-7.48 7.5c-4.1 0-7.5-3.4-7.5-7.5z"></path></svg>}
                                    suffix={<CloseOutlined />}
                                    placeholder="Search here"
                                />
                                <div className="Filter_Morefilter_body" >
                                    <Checkbox.Group style={{ width: '100%' }} onChange={handleMoreFilter} >
                                        <Row gutter={[0, 12]} >
                                            {plainOptions.map((val, index) => {
                                                return <Col key={index} xl={24} sm={24} xs={24}>
                                                    <Checkbox value={val}>{val}</Checkbox>
                                                </Col>
                                            })}
                                        </Row>
                                    </Checkbox.Group>
                                </div>
                            </div>
                        </TabPane>
                    </Tabs>
                </Modal>
            </div>
            <Row className="Row1" justify="center">
                <Col xl={18} sm={22} xs={22}>
                    <Title level={3} className="Title">
                        Collections
                    </Title>
                    <Row>
                        <Col>
                            <Paragraph>
                                Explore curated lists of top restaurants, cafes, pubs, and bars in Jodhpur, based on trends
                            </Paragraph>
                        </Col>
                    </Row>
                    <Carousel {...settings} >
                        {arr.map((val, index) => {
                            return <div key={index} className="carddiv">
                                <FoodCollectionCard obj={val} />
                            </div>
                        })}
                    </Carousel>
                </Col>
            </Row>
            <Affix offsetTop={0}>
                <Row justify="center" className="TabRow1">
                    <Col xl={18} sm={22} xs={22} className="Col1">
                        <Space className="Space">
                            <Button className="btn" onClick={showModal} icon={<svg xmlns="http://www.w3.org/2000/svg" fill="#9C9C9C" width="18" height="18" viewBox="0 0 20 20" aria-labelledby="icon-svg-title- icon-svg-desc-" role="img" class="sc-rbbb40-0 iwHbVQ"><title>filter</title><path d="M2.14 6.42h7.26c0.353 1.207 1.45 2.074 2.75 2.074s2.397-0.867 2.745-2.054l0.005-0.020h2.96c0.343-0.059 0.6-0.355 0.6-0.71s-0.258-0.651-0.596-0.709l-0.004-0.001h-2.94c-0.341-1.226-1.447-2.11-2.76-2.11s-2.419 0.885-2.755 2.090l-0.005 0.020h-7.26c-0.343 0.059-0.6 0.355-0.6 0.71s0.257 0.651 0.596 0.709l0.004 0.001zM12.16 4.28c0.776 0.011 1.4 0.643 1.4 1.42 0 0.784-0.636 1.42-1.42 1.42-0.777 0-1.409-0.624-1.42-1.399l-0-0.001c-0-0.006-0-0.013-0-0.020 0-0.784 0.636-1.42 1.42-1.42 0.007 0 0.014 0 0.021 0l-0.001-0zM17.86 13.58h-7.24c-0.328-1.245-1.443-2.148-2.77-2.148s-2.442 0.903-2.766 2.128l-0.004 0.020h-2.94c-0.036-0.006-0.077-0.010-0.12-0.010-0.398 0-0.72 0.322-0.72 0.72s0.322 0.72 0.72 0.72c0.042 0 0.084-0.004 0.124-0.011l-0.004 0.001h2.96c0.353 1.207 1.45 2.074 2.75 2.074s2.397-0.867 2.745-2.054l0.005-0.020h7.26c0.343-0.059 0.6-0.355 0.6-0.71s-0.258-0.651-0.596-0.709l-0.004-0.001zM7.84 15.72c-0.776-0.011-1.4-0.643-1.4-1.42 0-0.784 0.636-1.42 1.42-1.42 0.777 0 1.409 0.624 1.42 1.399l0 0.001c0 0.006 0 0.013 0 0.020 0 0.784-0.636 1.42-1.42 1.42-0.007 0-0.014-0-0.021-0l0.001 0z"></path></svg>}><span className="span">Filter</span></Button>
                            {tagsData?.tagdata0.map((obj) =>
                                <RenderHotTegs obj={obj} />
                            )}
                            <Popover content={<RenderCuisinePopover />} placement='bottomLeft' title={<Title className="MoreFilter_Title">Cuisines</Title>} trigger="click">
                                <Button className="btn">
                                    <span className="span">Cuisines</span> <DownOutlined />
                                </Button>
                            </Popover>
                            {tagsData?.tagdata1.map((obj) =>
                                <RenderHotTegs obj={obj} />
                            )}
                            <Popover content={<RenderMoreFilter />} placement='bottomRight' title={<Title className="MoreFilter_Title">More Filters</Title>} trigger="click">
                                <Button className="btn" ><span className="span">More Filter</span> <DownOutlined /></Button>
                            </Popover>
                            {tagsData?.tagdata2.map((obj) =>
                                <RenderHotTegs obj={obj} />
                            )}
                        </Space>
                    </Col>
                </Row>
            </Affix>
            <Row justify="center" className="Row2">
                <Col xl={18} sm={22} xs={22}>
                    <Title level={3} className="Title">
                        Dine-Out Restaurants in Basni
                    </Title>
                    <Row gutter={[28, 28]}>
                        {carddata.map((val, index) => {
                            return <Col sm={8} xl={8} xs={24} key={index}>
                                <FoodCardCategory data={val} />
                            </Col>
                        })}
                    </Row>
                </Col>
            </Row>
        </div>
    )
}

export default DiningOut
