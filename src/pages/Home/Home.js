import { Col, Row, Typography, Form, Select, Input, Space, Card, Radio, Button } from 'antd'
import React, { useState } from 'react'
import './Home.scss'
import { EnvironmentFilled, RightOutlined } from '@ant-design/icons'
import card1img from '../../assets/zomatocard1.webp'
import card2img from '../../assets/zomatocard2.webp'
import zomatoBg from '../../assets/ZomatoBg.avif'
import zomatotext from '../../assets/zomatotext.avif'
import cards1img from '../../assets/card1img.webp'
import cards2img from '../../assets/card2img.webp'
import cards3img from '../../assets/card3img.webp'
import cards4img from '../../assets/card4img.webp'
import zomatomobailbg from '../../assets/zomatomobailbg.avif'
import appstore1 from '../../assets/appstore1.webp'
import appstore2 from '../../assets/appstore2.webp'
import Headers from '../../components/Header/Header'
import Footers from '../../components/Footer/Footer'
import FoodCollectionCard from '../../components/FoodCollectionCard/FoodCollectionCard'
import { Link } from 'umi'

const { Meta } = Card;
const { Title, Paragraph } = Typography

const arr = [
    {
        img: cards1img,
        title: "Best of Jodhpur",
        place: "10 Places "
    },
    {
        img: cards2img,
        title: "Trading This Week",
        place: "25 Places "
    },
    {
        img: cards3img,
        title: "Sweet Tooth",
        place: "12 Places "
    },
    {
        img: cards4img,
        title: "Roayl Rajasthani Cuisine",
        place: "12 Places "
    }
]
const dataitem = [
    "Sardarpura (355 places)", "Chopanci Housing Bord", "Paota (355 places)", "Bansi (210 place)", "Ratanada (197 place)", "Shastri Nagar (163 place)", "Rawaton Ka Bass (105 Place)", "Pal Gaon (73 Place)", "Air Force Area (69 Place)", "1st Polita (60 place)", "Mandore (56 place)", "Kabir Nagar (56 place)", "Station Road (54 place)", "Cantt Area (46 place)", "Maderana Colony(41 place)", "Bjs Colony (33 place)", "Bhagt ki Kothi (33 place)", "Sangriya (25 Place)", "Ravat Nagar (22 place)", "Bhadu Market(17 Place)", "Prem Nagar (16 place)", "Lawaran (14 Place)", "Soor Sagar (7 Places)", "Sardarpura (355 places)"
]
const citys = ["Andhra Pradesh",
    "Arunachal Pradesh",
    "Assam",
    "Bihar",
    "Chhattisgarh",
    "Goa",
    "Gujarat",
    "Haryana",
    "Himachal Pradesh",
    "Jammu and Kashmir",
    "Jharkhand",
    "Karnataka",
    "Kerala",
    "Madhya Pradesh",
    "Maharashtra",
    "Manipur",
    "Meghalaya",
    "Mizoram",
    "Nagaland",
    "Odisha",
    "Punjab",
    "Rajasthan",
    "Sikkim",
    "Tamil Nadu",
    "Telangana",
    "Tripura",
    "Uttarakhand",
    "Uttar Pradesh",
    "West Bengal",
    "Andaman and Nicobar Islands",
    "Chandigarh",
    "Dadra and Nagar Haveli",
    "Daman and Diu",
    "Delhi",
    "Lakshadweep",
    "Puducherry"]
const itemArray = ["Bakery food near me", "Beverages food near me", "Biryani food near me", "Burger food near me", "Cafe food near me", "Chinese food near me", "Desserts food near me", "Ice food near me", "Italian food near me", "Mithai food near me", "Momos food near me", "Bakery food near me", "Beverages food near me", "Biryani food near me", "Burger food near me", "Cafe food near me", "Chinese food near me", "Desserts food near me", "Ice food near me", "Italian food near me", "Mithai food near me", "Momos food near me"]
function Home() {
    const [value, setValue] = useState(1)
    const handleChange = (e) => {
        setValue(e.target.value)
    }
    return (
        <div>
            <div className="homecontainer">
                <div><Headers /></div>
                <div className="div1">
                    <div className="innerdiv1">
                        <div className="low-res-container">
                            <img src={zomatoBg} className="high-res-image" alt="zomatobg" />
                        </div>
                    </div>
                </div>
                <Row className="Row1" justify="center">
                    <Col span="24" align="center">
                        <img src={zomatotext} className="low-res-image" alt="Zomato text" />
                        <Paragraph className="para">
                            Discover the best food & drinks in Jodhpur
                        </Paragraph>
                        <Row className="innerRow1" justify="center">
                            <Col xl={12} sm={22} xs={22}>
                                <div className="innerdiv">
                                    <Row>
                                        <Col xl={7} xs={24} sm={8} className="Col1" align="start">
                                            <div className="itemdiv">
                                                <EnvironmentFilled className="icon" />
                                                <Form className="form">
                                                    <Form.Item >
                                                        <Select placeholder="BJS Colony" >
                                                        </Select>
                                                    </Form.Item>
                                                </Form>
                                            </div>
                                        </Col>
                                        <Col xl={16} sm={16} xs={24} className="Col2">
                                            <span className="vrBorder"></span>
                                            <Space>
                                                <Form>
                                                    <Form.Item >
                                                        <Input size="large" placeholder="Search for restaurant, cuisine or a dish" prefix={<div className="input-prefix">
                                                            <svg xmlns="http://www.w3.org/2000/svg" fill="#828282" width="18" height="18" viewBox="0 0 20 20" aria-labelledby="icon-svg-title- icon-svg-desc-" role="img" class="sc-rbbb40-0 iwHbVQ"><title>Search</title><path d="M19.78 19.12l-3.88-3.9c1.28-1.6 2.080-3.6 2.080-5.8 0-5-3.98-9-8.98-9s-9 4-9 9c0 5 4 9 9 9 2.2 0 4.2-0.8 5.8-2.1l3.88 3.9c0.1 0.1 0.3 0.2 0.5 0.2s0.4-0.1 0.5-0.2c0.4-0.3 0.4-0.8 0.1-1.1zM1.5 9.42c0-4.1 3.4-7.5 7.5-7.5s7.48 3.4 7.48 7.5-3.38 7.5-7.48 7.5c-4.1 0-7.5-3.4-7.5-7.5z"></path></svg>
                                                        </div>} />
                                                    </Form.Item>
                                                </Form>
                                            </Space>
                                        </Col>
                                    </Row>
                                </div>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </div>
            <Row justify="center" className="outRow1">
                <Col xl={18} sm={22} xs={22}>
                    <Row gutter={[24, 24]} className="innerRow1">
                        <Col span="12">
                            <Link to='/tabs'>
                            <Card
                                hoverable
                                className="card"
                                cover={<img alt="example" className="img" src={card1img} />}
                            >
                                <Meta title={<Paragraph  style={{ margin: 0, padding: 0 }} ellipsis={{ rows: 1 }}>Order Food Online</Paragraph>} description={<Paragraph className="paras" style={{ margin: 0, padding: 0, fontSize: "14px" }} ellipsis={{ rows: 1 }}>Stay home and order to your doorstep</Paragraph>} />
                            </Card>
                            </Link>
                        </Col>
                        <Col span="12">
                            <Link to="/tabs">
                            <Card
                                hoverable
                                className="card"
                                cover={<img alt="example" className="img" src={card2img} />}
                            >
                                <Meta title={<Paragraph  style={{ margin: 0, padding: 0 }} ellipsis={{ rows: 1 }}>Go out for a meal</Paragraph>} description={<Paragraph className="paras" style={{ margin: 0, padding: 0, fontSize: "14px" }} ellipsis={{ rows: 1 }}>View the city's favourite dining venues</Paragraph>} />
                            </Card>
                            </Link>
                        </Col>
                    </Row>
                    <Row className="innerRow2">
                        <Col>
                            <Title className="title">
                                Collections
                            </Title>
                            <div>
                                <Paragraph className="para">
                                    Explore curated lists of top restaurants, cafes, pubs, and bars in Jodhpur, based on trends
                                </Paragraph>
                            </div>
                        </Col>
                    </Row>
                    <Row className="innerRow3" gutter={[12, 12]}>
                        {arr.map((val, index) => {
                            return <Col xl={6} sm={6} xs={12} key={index}>
                                <FoodCollectionCard obj={val} />
                            </Col>
                        })}
                    </Row>

                    <Row className="innerRow4" justify="center">
                        <Col>
                            <Title className="title" align="center">Popular localities in and around Jodhpur</Title>
                        </Col>
                    </Row>
                    <Row className="innerRow5" gutter={[24, 24]} justify="center">
                        {dataitem.map((val, index) => {
                            return <Col xl={8} sm={8} xs={22} key={index}>
                                <div className="div1">
                                    <Paragraph ellipsis={{rows:1}} className="paramain">
                                        {val}
                                    </Paragraph>
                                    <Paragraph className="para">
                                        <RightOutlined className="icon" />
                                    </Paragraph>
                                </div>
                            </Col>
                        })}
                    </Row>
                </Col>
            </Row>
            <Row className="outRow2" justify="center">
                <Col xl={14} sm={20} xs={24}>
                    <Row className="innerRow1">
                        <Col xl={8} sm={8} xs={8} className="Col1">
                            <div className="imgdiv">
                                <img src={zomatomobailbg} alt="mobailimg" className="img" />
                            </div>
                        </Col>
                        <Col xl={14} xs={22} sm={14} className="Col2">
                            <div className="Cold2iv1">
                                <Title className="title">
                                    Get the Zomato App
                                </Title>
                                <Paragraph className="para">
                                    We will send you a link, open it on your phone to download the app
                                </Paragraph>
                                <Row className="FormRow">
                                    <Col span="24">
                                        <Form className="Form"
                                            initialValues={
                                                { radiogroup: 1 }
                                            }
                                        >
                                            <Form.Item name="radiogroup">
                                                <Radio.Group value={value} onChange={handleChange}>
                                                    <Radio value={1}>Email</Radio>
                                                    <Radio value={2}>Phone</Radio>
                                                </Radio.Group>
                                            </Form.Item>
                                            <Row gutter={[12, 12]}>
                                                <Col xl={16} sm={18} xs={12}>
                                                    <Form.Item>
                                                        <Input placeholder="Email"></Input>
                                                    </Form.Item>
                                                </Col>
                                                <Col xl={8} sm={6} xs={12}>
                                                    <Form.Item>
                                                        <Button type="primary" className="btn">Share the app</Button>
                                                        <Button type="primary" className="btn-mobail">Share</Button>
                                                    </Form.Item>
                                                </Col>
                                            </Row>
                                        </Form>
                                        <Paragraph className="App_Download">
                                            Download app from
                                        </Paragraph>
                                        <Row gutter={[24, 24]}>
                                            <Col xl={8} sm={12} xs={12}>
                                                <img src={appstore1} alt="" className="img" />
                                            </Col>
                                            <Col xl={8} sm={12} xs={12}>
                                                <img src={appstore2} alt="" className="img" />
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                            </div>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row justify="center" className="outRow3">
                <Col xl={18} sm={22} xs={22}>
                    <Row>
                        <Col>
                            <Title className="Explore_title">Explore other options for you here</Title>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Title level={3} className='Comman_title'>
                                Popular cuisines near me
                            </Title>
                            <div className="itemdiv">
                                {itemArray.map((val, index) => {
                                    return <span key={index}>
                                        <a href="http://localhost:8000/#" className="atag">{val}</a>
                                        <span className="dots">
                                        </span>
                                    </span>
                                })}
                            </div>
                        </Col>
                    </Row>
                    <Row className="Row2">
                        <Col>
                            <Title level={3} className="Comman_title">
                                Popular restaurant types near me
                            </Title>
                            <div className="itemdiv">
                                {itemArray.map((val, index) => {
                                    return <span key={index}>
                                        <a href="http://localhost:8000/#" className="atag">{val}</a>
                                        <span className="dots">
                                        </span>
                                    </span>
                                })}
                            </div>
                        </Col>
                    </Row>
                    <Row className="Row3">
                        <Col>
                            <Title level={3} className="Comman_title"> 
                                Top Restaurant Chains
                            </Title>
                            <a href="http://localhost:8000/#" className="atag">Pizza hut</a>
                        </Col>
                    </Row>
                    <Row className="Row4">
                        <Col>
                            <Title level={3} className="Comman_title">
                                Cities We Deliver To
                            </Title>
                            <Row>
                                {citys.map((val, key) => {
                                    return <Col xl={4} sm={4} xs={12} key={key}>
                                        <Paragraph ellipsis={{ rows: 1 }} style={{ marginBottom: "16px",cursor:'pointer' }} href="http://localhost:8000/#" className="atag">{val}</Paragraph>
                                    </Col>
                                })}
                            </Row>
                        </Col>
                    </Row>

                </Col>
            </Row>
            <div>
                <Footers />
            </div>
        </div>
    )
}

export default Home
