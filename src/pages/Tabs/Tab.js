import { Col, Row, Tabs, Breadcrumb } from 'antd'
import React from 'react'
import DiningOut from '../DiningOut/DiningOut';
import NightLife from '../NightLife/NightLife';
import Order from '../Order/Order';
import './Tabs.scss'
import deliveryimg from '../../assets/deliveryimg.avif'
import DiningOutimg from '../../assets/DiningOut.avif'
import nightLifeimg from '../../assets/nightLife.webp'
import OrderComponent from '../../components/OrderComponent/OrderComponent';
import { StickyContainer, Sticky } from 'react-sticky';
import Footer from '../../components/Footer/Footer'

const { TabPane } = Tabs;
function Tab() {
    function callback(key) {
        console.log(key);
    }
    const renderTabBar = (props, DefaultTabBar) => (
        <Sticky bottomOffset={80}>
            {({ style }) => (
                <DefaultTabBar {...props} className="site-custom-tab-bar" style={{ ...style }} />
            )}
        </Sticky>
    );
    const RenderBreadcrumb = () => {
        return <Breadcrumb>
            <Breadcrumb.Item>
                <a className="link" href="http://google.com">Home</a>
            </Breadcrumb.Item>
            <Breadcrumb.Item>
                <a className="link" href="http://google.com">India</a>
            </Breadcrumb.Item>
            <Breadcrumb.Item>
                <a className="link" href="http://google.com">Jodhpur</a>
            </Breadcrumb.Item>
            <Breadcrumb.Item>
                <a className="link" href="http://google.com">Basni Restaurants </a>
            </Breadcrumb.Item>
        </Breadcrumb>
    }
    return (
        <div className="TabContainer">
            <Row justify="center" className="Header">
                <Col xl={18} sm={22} xs={22} style={{ position: "static" }}>
                    <OrderComponent />
                </Col>
            </Row>
            <Row justify="center" className="Row1tab">
                <Col xl={18} sm={22} xs={22}>
                    <div>
                        <RenderBreadcrumb />
                    </div>
                </Col>
            </Row>
            <StickyContainer>
                <Tabs defaultActiveKey="1" onChange={callback} className="Tabs" renderTabBar={renderTabBar}>
                    <TabPane className="tabpane1" tab={
                        //   <Link to='tabs/diningout'>
                        <span className="tabs1span span span1">
                            <div className="deliverydiv1">
                                <img src={deliveryimg} alt="" className="img" />
                            </div>
                            Delivery
                        </span>
                        // </Link>
                    } key="1">
                        <Order />
                    </TabPane>
                    <TabPane tab={
                        <span className='span'>
                            <div className="deliverydiv2">
                                <img src={DiningOutimg} alt="" className="img" />
                            </div>
                            Dining Out
                        </span>
                    } key="2">
                        <DiningOut />
                    </TabPane>
                    <TabPane tab={
                        <span className="span3 span">
                            <div className="deliverydiv3">
                                <img src={nightLifeimg} alt="" className="img" />
                            </div>
                            NightLife
                        </span>
                    } key="3">
                        <NightLife />
                    </TabPane>
                </Tabs>
            </StickyContainer>
            <Row>
                <Col>
                    <Footer />
                </Col>
            </Row>
        </div>
    )
}

export default Tab
